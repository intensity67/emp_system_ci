<?php
 
  class LoginModel extends CI_Model{


        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }


        public function get_users_by_login($username, $password)
        {
          
            $this->db->where('username', $username);
            $this->db->where('password', $password);
            
            $query = $this->db->get('users');

            return $query->row_array();

        }


  }


?>