<?php
 
  class Employee_model extends CI_Model{


        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }
 
        public function get_employee_list(){

                $query = $this->db->get('emp_details');
                
                return $query->result_array();
        }

        public function get_employee_info($employee_id){

                $this->db->where('employee_id', $employee_id);

                $query = $this->db->get('emp_details');
                
                return $query->row_array();
        }

  }


?>