<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class MY_Controller extends CI_Controller {

	public function __construct(){
 
    parent::__construct();
      
    $this->load->library('user_agent');

    if($this->session->userdata("account_info")['user_id'] === NULL){

      redirect("Login");

    }else{

      $this->user_account_id = $this->session->userdata("login_info")['user_account_id'];

    }

    //echo $this->session->userdata("user_account_id");
	}

    public function page_render($page, $data = ''){

      $this->load->view('layout/header');

      $this->load->view('layout/sidebars');

      $this->load->view($page, $data);
         
      $this->load->view('layout/footer');

     }

}