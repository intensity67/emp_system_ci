
<?php
 
	class PDF extends FPDF{

	public $requested_by_id, $requested_by_name, $requested_by_date;
	 

	function Header() {
  
			//Logo

			 //Arial bold 15
			$this->SetFont('Arial','B',15);
			//Move to the right
			$this->Cell(80);
			//Title
 			$this->Ln(20);
			$this->Cell(30,45,'Date: '. date("M, d Y") ,0,0,'C');

 			
			//Line break
			$this->SetFont('Arial','B',8);
			$this->Ln(20);
			
				
 				 

 
			$this->Ln(20);

	} 


	function EmptyInventory() {
  
			//Logo

			
			//Arial bold 15
			$this->SetFont('Arial','B',15);
			
			//Move to the right
			$this->Cell(80);
			
			//Title
 			$this->Ln(20);

			$this->Cell(75,45,'No Current Reports for this day... ', 0, 0,'C');

			//Line break
			$this->SetFont('Arial','B',8);
			$this->Ln(20);
			
			$this->Ln(20);

	} 
		// Load data
		 

		// Simple table	enrollees
		function BasicTable($data)
		{
		
		   $this->Cell(20,7,"#",1, 0,'C');
		   $this->Cell(40,7,"Product Name",1, 0,'C');
		   $this->Cell(40,7,"Initial Stocks", 1, 0,'C');
		   $this->Cell(40,7,"Stocks Sold", 1, 0,'C');
		   $this->Cell(40,7,"Stocks Left", 1, 0,'C');

			$this->Ln();
			  

		$this->SetFont('Arial','', 7);

		for($i= 0; isset($data[$i]); $i++) //* Display Rows of the Table
			{
			
 				$this->Cell(20,6,$data[$i]['nos'],1, 0,'C');
 				$this->Cell(40,6,$data[$i]['prodName'],1, 0,'C');
 				$this->Cell(40,6,$data[$i]['stocks_initial'],1, 0,'C');
  				$this->Cell(40,6,$data[$i]['stocks_sold'],1, 0,'C');
 				$this->Cell(40,6,$data[$i]['stocks_left'],1, 0,'C');
				$this->Ln();
			} 
			
			  	//* Display Columns of the Table
				 
				
		

		 
			 
			
			
		}

		// Page footer
		function Footer()
		{
			// Position at 1.5 cm from bottom
			$this->SetY(-15);
			// Arial italic 8
			$this->SetFont('Arial','I',8);
			// Page number
			$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
		}

		// Better table
		function ImprovedTable($header, $data)
		{
			// Column widths
			$w = array(40, 35, 40, 45);
			// Header
			for($i=0;$i<count($header);$i++)
				$this->Cell($w[$i],7,$header[$i],1,0,'C');
			$this->Ln();
			// Data
			foreach($data as $row)
			{
				$this->Cell($w[0],6,$row[0],'LR');
				$this->Cell($w[1],6,$row[1],'LR');
				$this->Cell($w[2],6,number_format($row[2]),'LR',0,'R');
				$this->Cell($w[3],6,number_format($row[3]),'LR',0,'R');
				$this->Ln();
			}
			// Closing line
			$this->Cell(array_sum($w),0,'','T');
		}

		// Colored table
		function FancyTable($header, $data)
		{
			// Colors, line width and bold font
			$this->SetFillColor(255,0,0);
			$this->SetTextColor(255);
			$this->SetDrawColor(128,0,0);
			$this->SetLineWidth(.3);
			$this->SetFont('','B');
			// Header
			$w = array(40, 35, 40, 45);
			for($i=0;$i<count($header);$i++)
				$this->Cell($w[$i],7,$header[$i],1,0,'C',true);
			$this->Ln();
			// Color and font restoration
			$this->SetFillColor(224,235,255);
			$this->SetTextColor(0);
			$this->SetFont('');

			$fill = false;
			foreach($data as $row)
			{
				$this->Cell($w[0],6,$row[0],'LR',0,'L',$fill);
				$this->Cell($w[1],6,$row[1],'LR',0,'L',$fill);
				$this->Cell($w[2],6,number_format($row[2]),'LR',0,'R',$fill);
				$this->Cell($w[3],6,number_format($row[3]),'LR',0,'R',$fill);
				$this->Ln();
				$fill = !$fill;
			}
			// Closing line
			$this->Cell(array_sum($w),0,'','T');
		}
	}

		$pdf = new PDF();
 
		$pdf->AddPage();

 		$i= 0;
	
				foreach( $inventory_report as $row){

 					$reports_row[$i]['nos'] =	$i + 1;

 					$reports_row[$i]['prodName'] 		=	$row->prodName;
 					$reports_row[$i]['stocks_initial'] =	$row->prodStocks;
 					$reports_row[$i]['stocks_sold'] =	$row->total_sold;
 					$reports_row[$i]['stocks_left'] =	$row->prodStocks - $row->total_sold;
					
					$i++;
				
				}
 
			
		//*For Background: $pdf->Image('img/background.png',0,15,-175);
		
		if(!empty($reports_row))

			$pdf->BasicTable($reports_row);
		else 

			$pdf->EmptyInventory();

		$pdf->SetFont('Arial','', 7);

		$pdf->Cell(0,10,'Date Printed:  '. date("M d Y"));
 
		$pdf->SetTitle('Inventory Report');

		$pdf->Output();


?>