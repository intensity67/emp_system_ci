<div class="row">

          <div style="background-image: url(<?php echo base_url('assets/dci-logo.png'); ?>)" id = "edit_employee_bg"></div>

          <div class="col-lg-7">

            <div class="p-5">

              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4"> Add Employee </h1>
              </div>

              <form class="user" action = "<?php echo base_url('Employee/insert_employee'); ?>" method="POST">

                <div class="form-group">

                  <label>Employee ID</label>

                  <input type="text" class="form-control form-control-user" placeholder="Employee ID">
                </div>

                <div class="form-group row">

                  <div class="col-sm-4 mb-3 mb-sm-0">
                    <label>First Name</label>

                    <input type="text" class="form-control form-control-user" placeholder="First Name">
                  </div>

                  <div class="col-sm-4">
                    <label>Middle Name</label>

                    <input type="text" class="form-control form-control-user"  placeholder="Middle Name">
                  </div>

                  <div class="col-sm-4">
                      <label>Last Name</label>

                      <input type="text" class="form-control form-control-user"  placeholder="Last Name">
                  </div>

                </div>
              
                
                <div class="form-group row">

                  <div class="col-sm-6">
                      <label>Batch</label>

                      <input type="text" class="form-control form-control-user"  placeholder="Batch">
                  </div>

                  <div class="col-sm-6">
                      <label>Site Id</label>

                      <input type="text" class="form-control form-control-user"  placeholder="Site Id">
                  </div>

                </div>
                
                <div class="form-group row">

                  <div class="col-sm-6">
                      <label>Position</label>

                      <input type="text" class="form-control form-control-user"  placeholder="Position">
                  </div>

                  <div class="col-sm-6">
                      <label>Status</label>

                      <input type="text" class="form-control form-control-user"  placeholder="Status">
                  </div>

                </div>
                
                <div class="form-group row">

                  <div class="col-sm-6">
                      <label>Department</label>

                      <input type="text" class="form-control form-control-user"  placeholder="Department">
                  </div>

                  <div class="col-sm-6">
                      <label>Nesting Date</label>

                      <input type="date" class="form-control form-control-user"  placeholder="Nesting Date">
                  </div>

                </div>

                <div class="form-group row">

                  <div class="col-sm-6">
                      <label>Evaluation Period</label>

                      <input type="text" class="form-control form-control-user"  placeholder="Evaluation Period">
                  </div>

                  <div class="col-sm-6">
                      <label>Reprofile Date</label>

                      <input type="date" class="form-control form-control-user"  placeholder="Reprofile Date">
                  </div>

                </div>

                <div class="form-group row">

                  <div class="col-sm-6">
                      <label>Training Extension</label>

                      <input type="text" class="form-control form-control-user"  placeholder="Training Extension">
                  </div>

                  <div class="col-sm-6">
                      <label>Supervisor </label>

                      <input type="text" class="form-control form-control-user"  placeholder="Supervisor">
                  </div>

                </div>

                <div class="form-group row">

                  <div class="col-sm-6">
                      <label>Job Date</label>

                      <input type="date" class="form-control form-control-user"  placeholder="Job Date">
                  </div>

                  <div class="col-sm-6">
                      <label>Start Date</label>

                      <input type="date" class="form-control form-control-user"  placeholder="Start Date">
                  </div>

                </div>

                <div class="form-group row">

                  <div class="col-sm-6">
                      
                      <label>Assoc. Date</label>
                      
                      <input type="date" class="form-control form-control-user"  placeholder="Assoc. Date">
                  
                  </div>

                  <div class="col-sm-6">
                    
                      <label>Consultant Date</label>

                      <input type="date" class="form-control form-control-user" placeholder="Consultant Date">
                  </div>

                </div>
 
                <div class="form-group row">

                  <div class="col-sm-6">
                      <label>5th Month Evaluation</label>

                      <input type="date" class="form-control form-control-user" placeholder="5th Month Evaluation">
                  </div>

                  <div class="col-sm-6">
                      <label>Reguralization Date</label>

                      <input type="date" class="form-control form-control-user"  placeholder="Reguralization Date">
                  </div>

                </div>

                </div>

                <button type = 'submit' class="btn btn-success btn-user btn-block">
                  Add Employee
                </button>
                
                <hr>

              </form>

             </div>

          </div>

        </div>