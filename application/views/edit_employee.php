<div class="row">

          <div style="background-image: url(<?php echo base_url('assets/dci-logo.png'); ?>)" id = "edit_employee_bg"></div>

          <div class="col-lg-7">

            <div class="p-5">

              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4"> Update Employee Details </h1>
              </div>

              <form class="user" action = "<?php echo base_url('Employee/insert_employee'); ?>" method="POST">

                <div class="form-group">

                  <label>Employee ID</label>

                  <input type="text" class="form-control form-control-user" placeholder="Employee ID" value="<?php echo $employee_info['employee_id']; ?>">
                </div>

                <div class="form-group row">

                  <div class="col-sm-4 mb-3 mb-sm-0">
                    <label>First Name</label>

                    <input type="text" class="form-control form-control-user" placeholder="First Name" 
                    value="<?php echo $employee_info['first_name']; ?>">
                  </div>

                  <div class="col-sm-4">
                    <label>Middle Name</label>

                    <input type="text" class="form-control form-control-user"  placeholder="Middle Name"  value="<?php echo $employee_info['middle_name']; ?>">
                  </div>

                  <div class="col-sm-4">
                      <label>Last Name</label>

                      <input type="text" class="form-control form-control-user"  placeholder="Last Name"  value="<?php echo $employee_info['last_name']; ?>">
                  </div>

                </div>
              
                
                <div class="form-group row">

                  <div class="col-sm-6">
                      <label>Batch</label>

                      <input type="text" class="form-control form-control-user"  placeholder="Batch"  value="<?php echo $employee_info['batch']; ?>">
                  </div>

                  <div class="col-sm-6">
                      <label>Site Id</label>

                      <input type="text" class="form-control form-control-user"  placeholder="Site Id"  value="<?php echo $employee_info['site_id']; ?>">
                  </div>

                </div>
                
                <div class="form-group row">

                  <div class="col-sm-6">
                      <label>Position</label>

                      <input type="text" class="form-control form-control-user"  placeholder="Position"  value="<?php echo $employee_info['position']; ?>">
                  </div>

                  <div class="col-sm-6">
                      <label>Status</label>

                      <input type="text" class="form-control form-control-user"  placeholder="Status"  value="<?php echo $employee_info['emp_status']; ?>">
                  </div>

                </div>
                
                <div class="form-group row">

                  <div class="col-sm-6">
                      <label>Department</label>

                      <input type="text" class="form-control form-control-user"  placeholder="Department"  value="<?php echo $employee_info['department']; ?>">
                  </div>

                  <div class="col-sm-6">
                      <label>Nesting Date</label>

                      <input type="text" class="form-control form-control-user"  placeholder="Nesting Date"  value="<?php echo $employee_info['nesting_date']; ?>">
                  </div>

                </div>

                <div class="form-group row">

                  <div class="col-sm-6">
                      <label>Evaluation Period</label>

                      <input type="text" class="form-control form-control-user"  placeholder="Evaluation Period"  value="<?php echo $employee_info['evaluation_period']; ?>">
                  </div>

                  <div class="col-sm-6">
                      <label>Reprofile Date</label>

                      <input type="text" class="form-control form-control-user"  placeholder="Reprofile Date"  value="<?php echo $employee_info['reprofile_date']; ?>">
                  </div>

                </div>

                <div class="form-group row">

                  <div class="col-sm-6">
                      <label>Training Extension</label>

                      <input type="text" class="form-control form-control-user"  placeholder="Training Extension"  value="<?php echo $employee_info['training_extension']; ?>">
                  </div>

                  <div class="col-sm-6">
                      <label>Supervisor </label>

                      <input type="text" class="form-control form-control-user"  placeholder="Supervisor"  value="<?php echo $employee_info['supervisor_id']; ?>">
                  </div>

                </div>

                <div class="form-group row">

                  <div class="col-sm-6">
                      <label>Job Date</label>

                      <input type="text" class="form-control form-control-user"  placeholder="Job Date"  value="<?php echo $employee_info['jo_date']; ?>">
                  </div>

                  <div class="col-sm-6">
                      <label>Start Date</label>

                      <input type="text" class="form-control form-control-user"  placeholder="Start Date"  value="<?php echo $employee_info['start_date']; ?>">
                  </div>

                </div>

                <div class="form-group row">

                  <div class="col-sm-6">
                      
                      <label>Assoc. Date</label>
                      
                      <input type="text" class="form-control form-control-user"  placeholder="Assoc. Date"  value="<?php echo $employee_info['assoc_date']; ?>">
                  
                  </div>

                  <div class="col-sm-6">
                    
                      <label>Consultant Date</label>

                      <input type="text" class="form-control form-control-user"  placeholder="Consultant Date"  value="<?php echo $employee_info['consultant_date']; ?>">
                  </div>

                </div>
 
                <div class="form-group row">

                  <div class="col-sm-6">
                      <label>5th Month Evaluation</label>

                      <input type="text" class="form-control form-control-user" placeholder="5th Month Evaluation"  value="<?php echo $employee_info['5th_month_evaluation']; ?>">
                  </div>

                  <div class="col-sm-6">
                      <label>Reguralization Date</label>

                      <input type="text" class="form-control form-control-user"  placeholder="Reguralization Date"  
                      value="<?php echo $employee_info['reguralization_date']; ?>">
                  </div>

                </div>

                </div>

                
                <button type = 'submit' class="btn btn-success btn-user btn-block">
                  Update Employee Details
                </button>
                
                <hr>

              </form>

             </div>

          </div>

        </div>