<div id="edit_employee_<?php echo $emp_id; ?>" class="modal fade in" role="dialog" aria-hidden="false" >

              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">

                  <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">×</button>

                    <h4 class="modal-title"> Edit Document Entry </h4>

                  </div>

                  <form action = "<?php echo base_url('Admin/update_document_entry'); ?>" method="POST">

                  <div class="modal-body">

                    <table class="table table-striped">
                        
                            <tbody>

                              <tr><td> First Name: <td> <input type="text" class="form-control" name = "first_name" value="<?php echo $first_name; ?>" >
 
                            </tbody>

                      </table>

                   </div>
                  

                  <div class="modal-footer">

                      <input type="submit" class="btn btn-success" value="Update Entry"  >

                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                  </div>

                  </form>

                </div>

              </div>

</div> 