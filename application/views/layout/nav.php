<?php
//if ($this->session->userdata('account_info')['acct_id'] == try) { ?>
<div class="navbar-header" >
    <a class="navbar-brand" href="<?php echo base_url(''); ?>"> Employee Directory System </a>
        <p class="navbar-brand"> (Login As:  Admin) </p>
    </div>
            <div class="navbar-default sidebar" role="navigation">

                <div class="sidebar-nav navbar-collapse" >

                    <ul class="nav" id="side-menu" >


                        <li><a href = "<?php echo base_url('HeadController/dashboard'); ?>"> Dashboard </a></li>
                        
                        <li><a class="get_notify" href = "<?php echo base_url('HeadController/document_lists'); ?>"> Employee List <span class="request badge" style="background-color:#ad0c0c; color:#fcf8e3;"></a></li>

                        <li><a href = "<?php echo base_url('HeadController/department'); ?>">  Departments  <span class="fa arrow"></span> </a>
                                <ul class="nav nav-second-level">
                                    <li>       
                                        <a href= "<?php echo base_url('HeadController/departments_lists'); ?>">  <i class = 'fa fa-list'> </i> Team Lists 
                                        </a>  

                                    </li>
 
                                </ul>

                        </li>

                        <li><a href = "<?php echo base_url('HeadController/section'); ?>">  Sections  <span class="fa arrow"></span> </a>
                        
                                <ul class="nav nav-second-level">


                                    <li> 
                                        
                                        <a href= "<?php echo base_url('HeadController/section_lists'); ?>">  <i class = 'fa fa-list'> </i> Sections Lists 
                                        </a>  

                                    </li>
 
                                </ul>

                        </li>

                        <li><a href = "<?php echo base_url('index.php/LoginController/logout'); ?>">Log- Out</a></li>

                    </ul>

                </div>

                <!-- /.sidebar-collapse -->
            </div>
    