<!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
            </div>

            <div class="form-group">

            <label>Status:</label>  

              <select name="status" class="form-control">
                  <option value="Active">Active</option>
                  <option value="Inactive">Inactive</option>
              </select>
            
            </div>

            <div class="form-group">

            <label>Department:</label>  

              <select name="department" class="form-control"> 

                  <option value="Admin">Admin</option>
                  <option value="Operations">Operations</option>
                  <option value="Administration">Administration</option>
                  <option value="FCL">FCL</option>
                  <option value="Insurance">Insurance</option>
                  <option value="Quality Assurance">Quality Assurance</option>
                  <option value=" IT">  IT</option>
              </select>
            
            <label>Position:</label>  

              <select name = "position" class="form-control"> 

                  <option value="Lead Generation Specialist">Lead Generation Specialist  </option>
                  <option value="Quality Assurance">Quality Assurance</option>
                  <option value="IT Specialist">  IT Specialist </option>
                  <option value="Facilities and Network">Facilities and Network</option>
                  <option value="Team Leader">Team Leader</option>
                  <option value="Admin Officer">Admin Officer</option>

              </select>
            
            <label>Batch:</label>  

              <select name="" class="form-control"> 
                  <option value="Wave 1">Wave 1</option>
                  <option value="Wave 2">Wave 2</option>
                  <option value="Wave 3">Wave 3</option>
                  <option value="Wave 4">Wave 4</option>
                  <option value="Wave 5">Wave 5</option>
                  <option value="Wave 6">Wave 6</option>
                  <option value="Wave 7">Wave 7</option>
                  <option value="Wave 11">Wave 11</option>
                  <option value="Wave 12">Wave 12</option>
                  <option value="Wave 13">Wave 13</option>
                  <option value="Wave 14">Wave 14</option>
                  <option value="Wave 15">Wave 15</option>

              </select>
            
            <label>Sites:</label>  

              <select name="site" class="form-control"> 
                  <option value="NY">NY</option>
                  <option value="OKC">OKC</option>
              </select>

            </div>

            <label>Contract Type:</label>  

              <select name="contract_type" class="form-control"> 

                  <option value="Independent Contractor">Independent Contractor</option>
                  <option value="Regularization Contract">Regularization Contract</option>
                  <option value="Probationary">Probationary</option>
                  <option value="Training Memo">Training Memo</option>
                  <option value="Training Contract">Training Contract</option>

              </select>

            </div>
            
            <h6 class="m-0 font-weight-bold text-primary">Employee Master List</h6>

            <div class="card-body">
              
              <a href="<?php echo base_url('Employee/add_employee'); ?>" class="btn btn-success"> Add Employee </a>

              <br>

              <div class="table-responsive">
              
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                 
                  <thead>
                 
                    <tr>
                      <th> Employee Id
                      <th> Name 
                      <th> Batch 
                      <th> Site 
                      <th> Position 
                      <th> Department
                      <th> Contract Type
                      <th> Status
                      <th>
                    </tr>

                  </thead>

                  <tfoot>
                    <tr>
                      <th> Employee Id
                      <th> Name 
                      <th> Batch 
                      <th> Site 
                      <th> Position 
                      <th> Department
                      <th> Contract Type
                      <th> Status
                      <th>  
                    </tr>

                  </tfoot>

                  <tbody>

                  <?php foreach($employee_list as $row): ?>

                    <tr>

                      <td><?php echo $row['employee_id']; ?>
                      <td><?php echo $row['first_name']. ' ' . $row['middle_name']. ' ' . $row['last_name']; ?>
                      <td><?php echo $row['batch']; ?>
                      <td><?php echo $row['site_id']; ?>
                      <td><?php echo $row['position']; ?>
                      <td><?php echo $row['department']; ?>
                      <td><?php echo $row['contract_type']; ?>
                      <td><?php echo $row['emp_status']; ?>

                      <td>
                        <a href="<?php echo base_url('Employee/edit_employee/'.$row['employee_id']); ?>" class="btn btn-success">
                          
                          <i class="fa fa-edit"></i>
                          
                          </a>
            
                    </tr>

                  <?php endforeach; ?>
                   
                  </tbody>

                </table>

              </div>

            </div>

          </div>

        </div>
        <!-- /.container-fluid -->

      </div>

      <!-- End of Main Content -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>
