<!DOCTYPE html>

<html lang="en">

  <head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <meta name="description" content="">

    <meta name="author" content="">
 
    <title> LeadGen- HRIS System </title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('assets/sb-admin2/css/sb-admin-2.css'); ?> " rel="stylesheet">
  
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<!--     <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
 -->
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<!--     <script src="<?php echo base_url('assets/js/ie-emulation-modes-warning.js'); ?>"></script>
 -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>

<body class="bg-gradient-primary">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">

          <div class="card-body p-0">

            <!-- Nested Row within Card Body -->
            <div class="row">

              <div class="col-lg-6 d-none d-lg-block" style="background-image: url(<?php echo base_url('assets/dci-logo.png'); ?>)" id = "sign_in_form"></div>

              <div class="col-lg-6">

                <div class="p-5">

                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Login </h1>
                  </div>

                  <form class="user" action="<?php echo base_url('Login/login_check'); ?>" method = "POST">

                    <div class="form-group">
                      <input type="text" class="form-control form-control-user"  aria-describedby="emailHelp" placeholder="Enter Your Employee ID..." name = "username">
                    </div>

                    <div class="form-group">
                      <input type="password" class="form-control form-control-user" placeholder="Password" name = "password">
                    
                    </div>

                    <div class="form-group">
                      <div class="custom-control custom-checkbox small">
                        <input type="checkbox" class="custom-control-input" id="customCheck">
                        <label class="custom-control-label" for="customCheck">Remember Me</label>
                      </div>
                    
                    </div>

                    <button type="submit" class="btn btn-primary btn-user btn-block">
                      Login
                    </button>

                    <hr>

                  <div class="text-center">
                    <a class="small" href="#">Forgot Password?</a>
                  </div>

                  </form>

                  <hr>

                </div>

              </div>

            </div>

          </div>

        </div>

      </div>

    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

</body>

</html>
