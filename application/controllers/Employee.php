<?php


defined('BASEPATH') OR exit('No direct script access allowed');


class Employee extends My_Controller {

	public function __construct(){
 
			parent::__construct();
                // Your own constructor code
 
    }


    public function employee_list(){
 
        $data['employee_list']     =   $this->Employee_model->get_employee_list();

        $page = 'employee_list';

        $this->page_render($page, $data);

     }

    public function edit_employee($employee_id){

        $data['employee_info']     =   $this->Employee_model->get_employee_info($employee_id);

        $page = 'edit_employee';   

        $this->page_render($page, $data);

    }

    public function add_employee(){

        $page = 'add_employee'; 
        
        $data = '';

        $this->page_render($page, $data);

    }


 }